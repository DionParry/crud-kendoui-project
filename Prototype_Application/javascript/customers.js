var customers = [{
    CustomerID : 1,
    CategoryID : 1,
    Name : "College Motors",
    Address: "my aaddress",
    Postcode : "LL57 89A",
    TelephoneNumber : 07998767676,
    Email : "CollegeMotors@gmail.com",
    JoinDate : "07/10/3006",
    LeftDate : "",
    Category : {
        CategoryID : 1,
        CategoryName : "Companies",
        Description : "Company details"
    }
}, {
    CustomerID : 3,
    CategoryID : 2,
    Name : "James",
    Address: "my aaddress b",
    Postcode : "LL57 79A",
    TelephoneNumber : 07998767676,
    Email : "James@gmail.com",
    JoinDate : "07/10/3006", //usa format
    LeftDate : "",
    Category : {
        CategoryID : 2,
        CategoryName : "Client",
        Description : "Client details"
    }
}, {
    CustomerID : 2,
    CategoryID : 1,
    Name : "RF Motors",
    Address: "my aaddress",
    Postcode : "LL57 89B",
    TelephoneNumber : "07093867676",
    Email : "RFMotors@gmail.com",
    JoinDate : "06/17/2006", //month-day-year
    LeftDate : "01/18/2017",
    Category : {
        CategoryID : 1,
        CategoryName : "Companies",
        Description : "Company details"
    }
}];

//Retreived input data from input field page and stores it
var submit = document.getElementById("submit");
var data = [];
var customerID = 3;
submit.onclick = function() {
  var name = document.getElementById("name").value;
  var address = document.getElementById("address").value;
  var postcode = document.getElementById("postcode").value;
  var telephoneNumber = document.getElementById("telephoneNumber").value;
  var email = document.getElementById("email").value;
  var joinDate = "";
  var leftDate = "";
  data.push(customerID++, 1, name, address, postcode, telephoneNumber, email, joinDate, leftDate);
  alert(data);
  customers.push(data);
};
//input data is pushed into customer_data table
